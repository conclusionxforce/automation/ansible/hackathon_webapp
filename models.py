from app import db


class Visitors(db.Model):
    site_id = db.Column(db.Integer, primary_key=True)
    site_name = db.Column(db.String(64), index=True, unique=True)
    visitor_count = db.Column(db.Integer)

    def __init__(self, site_name, visitor_count):
        self.site_name = site_name
        self.visitor_count = visitor_count

    def __repr__(self):
        return '<site_name {}>'.format(self.site_name)
