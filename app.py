import redis
from flask import Flask, render_template, jsonify, flash, redirect, url_for
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap5

from config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bootstrap = Bootstrap5(app)

from models import Visitors

with app.app_context():
    db.create_all()

    if not db.session.query(Visitors).filter_by(site_name='test').first():
        db.session.add(Visitors("test", 0))
        db.session.commit()

cache = redis.StrictRedis(host=Config.CACHE_HOST, port=6379, username=Config.CACHE_USERNAME, password=Config.CACHE_PASSWORD)


@app.route('/')
def index():
    if cache.exists('visitor_count'):
        cache.incr('visitor_count')
        count = (cache.get('visitor_count')).decode('utf-8')
        db.session.query(Visitors). \
            filter(Visitors.site_name == 'test'). \
            update({'visitor_count': count})
        db.session.commit()
    else:
        cache_refresh = db.session.query(Visitors.visitor_count).filter_by(site_name='test').first()
        count = int(cache_refresh[0])
        cache.set('visitor_count', count)
        cache.incr('visitor_count')
        count = (cache.get('visitor_count')).decode('utf-8')
        db.session.query(Visitors). \
            filter(Visitors.site_name == 'test'). \
            update({'visitor_count': count})
        db.session.commit()
    return render_template('index.html', count=count)


@app.route('/resetcounter')
def resetcounter():
    cache.delete('visitor_count')
    db.session.query(Visitors). \
        filter(Visitors.site_name == 'test'). \
        update({'visitor_count': 0})
    db.session.commit()
    flash('Counters are reset')
    return redirect(url_for('index'))


@app.route('/health')
def health():
    return jsonify(health="UP")


if __name__ == '__main__':
    app.run()
