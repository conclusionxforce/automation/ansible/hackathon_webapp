Page Views - Flask App
==========================

Deze Applicatie houdt bij hoe vaak de webpagina bezocht is.
De applicatie kent twee routes:
- / laat een statische pagina zien
- /resetcounter zet de visitor-count op 0 -> redirect naar /
- /health 

De applicatie heeft de volgende afhankelijkheden
- [PostgreSQL](https://www.postgresql.org/)
- [Redis](https://redis.io/)

De aplicatie is gemaakt met [Flask](http://flask.pocoo.org/).

Applicatie
----------
Applicatie draait op poort `:5000`

Deployment
- Install python
```commandline
sudo dnf install python3 git libpq-devel libpq python3-devel gcc
```

- Clone the project 
```commandline
git clone https://gitlab.com/conclusionxforce/automation/ansible/hackathon_webapp.git
cd hackathon_webapp
```

- Install application requirements
```commandline
python3 -m venv .venv
source .venv/bin/activate
pip install --upgrade pip
(.venv)$ pip install -r requirements.txt
```

- Environment variables
```commandline
export FLASK_APP=app.py

export DATABASE_USERNAME=webapp
export DATABASE_PASSWORD=password
export DATABASE_DB=dbPageViews
export DATABASE_HOST=localhost

export CACHE_USERNAME=webapp
export CACHE_PASSWORD=secret
export CACHE_HOST=localhost
```

- Start
```commandline
gunicorn -b localhost:5000 -w 4 app:app 
```

Postgres
--------
De applicatie verwacht de volgende environment variable.

**Voorbeeld**

    - DATABASE_HOST=localhost
    - DATABASE_USERNAME=postgres
    - DATABASE_PASSWORD=dbPageViews
    - DATABASE_DB=dbPageViews

Verder verwacht de applicatie dat het `dbPageViews` schema in de database aanwezig is. 
Inrichting van de database wordt vanuit de applicatie gedaan.


**Development purpose**
```commandline
podman run --name some-postgres \
    -p 5432:5432 \
    -e POSTGRES_USER=webapp \
    -e POSTGRES_PASSWORD=password \
    -e POSTGRES_DB=dbPageViews \
    -d postgres
    
podman exec -it some-postgres sh

psql -U webapp -d dbPageViews    
```

Redis
--------
De applicatie verwacht de volgende environment variable.

**Voorbeeld**

    - CACHE_HOST=localhost
    - CACHE_USERNAME=webapp
    - CACHE_PASSWORD=secret

**Authenticatie $ Authorizatie**

Binnen Redis installatie dient een user te worden aangemaakt met rechten op de `visitor_count`.
Voorbeeld aan de hand van bovenstaande environment variable.
```commandline
ACL SETUSER webapp
ACL SETUSER webapp on >secret ~visitor_count +get +set +exists +incrby +del
```

**Development purpose**
```commandline
podman run --name some-redis \
    -p 6379:6379 \
    -d redis

podman exec -it some-redis redis-cli
    ACL SETUSER webapp
    ACL SETUSER webapp on >secret ~visitor_count +get +set +exists +incrby +del
```

- Workshops key/value store
```commandline
podman run --name some-redis \
    -p 6379:6379 \
    -d redis

podman exec -it some-redis redis-cli
    ACL SETUSER ansible
    ACL SETUSER ansible on >secret
    ACL SETUSER ansible +get +hget +hgetall
    ACL SETUSER ansible ~postgres* 
    ACL SETUSER ansible ~redis*
    
    HSET postgres username webapp
    HSET postgres password password
    HSET redis username webapp
    HSET redis password secret    
```

