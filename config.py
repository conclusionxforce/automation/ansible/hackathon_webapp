import os


class Config(object):
    DEBUG = False
    CSRF_ENABLED = True
    SECRET_KEY = 'very-secret-value'

    DATABASE_USERNAME = os.environ.get('DATABASE_USERNAME') or "webapp"
    DATABASE_PASSWORD = os.environ.get('DATABASE_PASSWORD') or "password"
    DATABASE_DB = os.environ.get('DATABASE_DB') or "dbPageViews"
    DATABASE_HOST = os.environ.get('DATABASE_HOST') or "localhost"
    DATABASE_URI = "postgresql+psycopg2://" + DATABASE_USERNAME + ":" + DATABASE_PASSWORD + "@" + DATABASE_HOST + "/" + DATABASE_DB
    SQLALCHEMY_DATABASE_URI = DATABASE_URI
    # SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://webapp:password@127.0.0.1/dbPageViews"

    CACHE_USERNAME = os.environ.get('CACHE_USERNAME') or "webapp"
    CACHE_PASSWORD = os.environ.get('CACHE_PASSWORD') or "secret"
    CACHE_HOST = os.environ.get('CACHE_HOST') or "localhost"

